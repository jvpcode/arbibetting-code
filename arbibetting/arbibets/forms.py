# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm,SetPasswordForm, PasswordChangeForm
from arbibets.models import UserProfile
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth import authenticate
import unicodedata
from django.utils.text import capfirst
from django.utils.translation import gettext, gettext_lazy as _
from django.conf import settings
from django.contrib.auth import get_user_model

usermodel= get_user_model()

class MyRegistrationForm(UserCreationForm):
     email = forms.EmailField(required=True)
     class Meta:
          model = User
          fields = ('username', 'email', 'password1', 'password2')
     def clean_email(self):
          data = self.cleaned_data['email']
          if User.objects.filter(email=data).exists():
               raise forms.ValidationError("This email already used")
          return data
     def clean_password1(self):
          passwd = self.cleaned_data.get('password1')
          passwd2= self.cleaned_data.get('password2')
          if passwd and len(passwd) < 6:
               raise forms.ValidationError('password too short.')
          nums=0
          chars=0
          for char in passwd:
               try:
                    int(char)
                    nums+=1
               except ValueError:
                    if char in u'!"#$%&/()=?*;:{}]ˇ~.,+':
                         raise forms.ValidationError('Only numbers and letters and underscore allowed')
                    else:
                         chars+=1
          if chars<1 or nums<1:
               raise forms.ValidationError('Password must contain at least one letter and one number')               
          # if passwd != passwd2:
               # raise forms.ValidationError('Password missmatch')               
          return passwd

     def save(self, commit=True):
          user = super(MyRegistrationForm, self).save(commit=False)
          user.email = self.cleaned_data['email']

          if commit:
               user.save()
          return user
          
class UserProfileForm(forms.ModelForm):
     class Meta:
          model = UserProfile
          fields = ('first_name', 'last_name', 'adress','city', 'state','country',)

          
class PasswordResetForm(forms.Form):
    email = forms.EmailField(label=_("Email"), max_length=254)
    UserModel= get_user_model()
    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):
        """
        Send a django.core.mail.EmailMultiAlternatives to `to_email`.
        """
        subject = loader.render_to_string(subject_template_name, context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)

        email_message = EmailMultiAlternatives(subject, body, from_email, [to_email])
        if html_email_template_name is not None:
            html_email = loader.render_to_string(html_email_template_name, context)
            email_message.attach_alternative(html_email, 'text/html')

        email_message.send()

    def get_users(self, email):
        """Given an email, return matching user(s) who should receive a reset.
        This allows subclasses to more easily customize the default policies
        that prevent inactive users and users with unusable passwords from
        resetting their password.
        """
        active_user= User.objects.get(email=email)
        return active_user

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name=None,
             extra_email_context=None):
        """
        Generate a one-use only link for resetting password and send it to the
        user.
        """
        email = self.cleaned_data["email"]
        user=self.get_users(email)
        if not domain_override:
            current_site = get_current_site(request)
            site_name = current_site.name
            domain = current_site.domain
        else:
            site_name = domain = domain_override
        context = {
            'email': email,
            'domain': domain,
            'site_name': site_name,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
            'user': user,
            'token': token_generator.make_token(user),
            'protocol': 'https' if use_https else 'http',}
        if extra_email_context is not None:
            context.update(extra_email_context)
        self.send_mail(
            subject_template_name, email_template_name, context, from_email,
            email, html_email_template_name=html_email_template_name,
        )



class SetPasswordForm2(SetPasswordForm):

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password2 and len(password1) < 6:
            raise forms.ValidationError('password too short.')
        nums=0
        chars=0
        for char in password2:
            try:
                int(char)
                nums+=1
            except ValueError:
                if char in u'!"#$%&/()=?*;:{}]ˇ~.,+':
                      raise forms.ValidationError('Only numbers and letters and underscore allowed')
                else:
                    chars+=1
        if chars<1 or nums<1:
               raise forms.ValidationError('Password must contain at least one letter and one number')               
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password2
          
class PasswordChangeForm2(PasswordChangeForm):
     def clean_new_password2(self):
          password1 = self.cleaned_data.get('new_password1')
          password2 = self.cleaned_data.get('new_password2')
          if password2 and len(password1) < 6:
               raise forms.ValidationError('password too short.')
          nums=0
          chars=0
          for char in password2:
               try:
                    int(char)
                    nums+=1
               except ValueError:
                    if char in u'!"#$%&/()=?*;:{}]ˇ~.,+':
                         raise forms.ValidationError('Only numbers and letters and underscore allowed')
                    else:
                         chars+=1
          if chars<1 or nums<1:
               raise forms.ValidationError('Password must contain at least one letter and one number')               
          if password1 and password2:
               if password1 != password2:
                    raise forms.ValidationError(
                         self.error_messages['password_mismatch'],
                         code='password_mismatch',
                    )
          return password2


from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^accounts/login/$', views.login, name='login'),
    url(r'^accounts/loggedin/$', views.index, name='index'),
    url(r'^about/$', views.about, name='about'),
    url(r'^accounts/auth_view/$', views.auth_view, name='auth_view'),
    url(r'^accounts/logout/$', views.logout, name='logout'),
	url(r'^accounts/profile_edit/$', views.user_profile_edit, name='user_profile_edit'),
	url(r'^accounts/profile/$', views.user_profile, name='user_profile'),
	url(r'^arbibets/profile/$', views.user_profile, name='user_profile'),
    url(r'^accounts/invalid_login/$', views.invalid_login, name='invalid_login'),
    url(r'^accounts/register/$', views.register_user, name='register_user'),
	url(r'^accounts/activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate, name='activate'),
    url(r'^password_reset/$', views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', views.password_reset_complete, name='password_reset_complete'),
	url(r'^password_change/$', views.password_change, name='password_change'),
	url(r'^password_change_done/$', views.password_change_done, name='password_change_done'),
	url(r'^purchase_premium/$', views.buy_premium, name='buy_premium'),
	url(r'^payment_failed/$', views.payment_failed, name='payment_failed'),
	url(r'^purchase_successful/$', views.payment_successful, name='payment_successful'),
	url(r'^google73cdd85a4713cc07.html$', views.google, name='google'),
]

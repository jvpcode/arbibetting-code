# -*- coding: utf-8 -*-
import datetime
from django.db import models
from django.contrib.auth.models import User
# from django.db.models.signals import post_save
# from django.dispatch import receiver
from paypal.standard.models import ST_PP_COMPLETED
from paypal.standard.ipn.signals import valid_ipn_received
from arbibets.signals import show_me_the_money



class Bet(models.Model):
    percentage = models.DecimalField(max_digits=5,decimal_places=3)
    player1name = models.CharField(max_length=120)
    player1odds = models.DecimalField(max_digits=5,decimal_places=2)
    player2name = models.CharField(max_length=120)
    player2odds = models.DecimalField(max_digits=5,decimal_places=2)
    tournament = models.CharField(max_length=140)
    matchtime = models.DateTimeField()
    player1bookie = models.CharField(max_length=59)
    player2bookie = models.CharField(max_length=59)

    def __str__(self):
        return (str(self.player1name)+':'+str(self.player2name))



class UserProfile(models.Model):
	user = models.OneToOneField(User,on_delete=models.CASCADE)
	first_name= models.CharField(max_length=120, blank=True,)
	last_name = models.CharField(max_length=120, blank=True, )
	adress = models.CharField(max_length=220, blank=True,)
	city = models.CharField(max_length=220, blank=True, )
	state = models.CharField(max_length=220, blank=True, )
	country = models.CharField(max_length=120, blank=True, )
	paid=models.DateField(auto_now_add=True)

User.profile=property(lambda u:UserProfile.objects.get_or_create(user=u)[0])
	
class Kupnja(models.Model):
	user=models.ForeignKey(User,on_delete=models.CASCADE)
	length=models.CharField(max_length=220, blank=True)
	date=models.DateTimeField(auto_now_add=True)
	paypal_invoice = models.CharField(max_length=127, blank=True)
	arbibets_invoice= models.CharField(max_length=127, blank=True)
	def __str__(self):
		return str(self.user.username + ',' + self.length + ',' + str(self.date) + '. Paypal invoice is: ' + str(self.paypal_invoice) + ' and arbibets_invoice is: ' + str(self.arbibets_invoice))
# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
    # if created:
        # Profile.objects.create(user=instance)

# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
    # instance.profile.save()

valid_ipn_received.connect(show_me_the_money)	
	
	

	
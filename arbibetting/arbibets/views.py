from django.shortcuts import render, render_to_response, resolve_url
from django.http import HttpResponseRedirect, HttpResponse
from arbibets.models import Bet
from django.contrib import auth
import warnings, base64
from django.contrib.auth import update_session_auth_hash
from django.conf import settings
from django.template.context_processors import csrf
from django.contrib.sites.shortcuts import get_current_site
from django.contrib import messages
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.shortcuts import render, redirect
from arbibets.forms import MyRegistrationForm, UserProfileForm, PasswordResetForm, SetPasswordForm2, PasswordChangeForm2
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.models import User
from django.core.mail import send_mail, EmailMessage
import logging
from django.core.mail import get_connection
import datetime
from django.views.decorators.cache import never_cache
from django.views.decorators.debug import sensitive_post_parameters
# from django.core.mail.message import EmailMessage
from django.views.decorators.csrf import csrf_protect
from django.utils.translation import gettext_lazy as _
from django.template.response import TemplateResponse
from paypal.standard.forms import PayPalPaymentsForm
from paypal.standard.models import ST_PP_COMPLETED
from paypal.standard.ipn.signals import valid_ipn_received
from arbibets.models import Kupnja

logr = logging.getLogger(__name__)

# Create your views here.

def index(request):
    today=datetime.date.today()
    if request.user.is_authenticated:
        if request.user.profile.paid > today:
            bets=Bet.objects.order_by('-percentage')
            return render(request,'arbibets/index_log.html',{'bets':bets})
        if request.user.profile.paid<= today:
            bets=Bet.objects.filter(percentage__lte= 0.5).order_by('-percentage')[0:15]
            return render(request,'arbibets/index_log.html',{'bets':bets})

    else:
        return render( request,'arbibets/index.html')
def register_success(request):
    return render_to_response('arbibets/register_success.html')

        
def login(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('arbibets/login.html',c)

def auth_view(request):
    username = request.POST.get('username','')
    password = request.POST.get('password','')
    user = auth.authenticate(username=username, password=password)

    if user is not None:
        auth.login(request, user)
        return HttpResponseRedirect('/accounts/loggedin/')
    else:
        return HttpResponseRedirect('/accounts/invalid_login/')

def loggedin(request):
    return redirect('index')

def invalid_login(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('arbibets/invalid_login.html',c)

def logout(request):
    auth.logout(request)
    return redirect('index')

def register_user(request):

    if request.method == 'POST':
        form = MyRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            mail_subject = 'Activate your arbibets account.'
            message = render_to_string('arbibets/acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token':account_activation_token.make_token(user),})
            to_email = form.cleaned_data.get('email')
            email = send_mail(mail_subject, message,'username@domain.com', [to_email], fail_silently=False,)
            return render_to_response('arbibets/register_success.html')
            # return HttpResponse('Please confirm your email address to complete the registration')
    else:
        form= MyRegistrationForm()
    return render(request, 'arbibets/register.html', {'form':form})


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return render(request,'arbibets/activation_success.html')
    else:
        return render(request, 'arbibets/activation_fail.html')
@login_required
def user_profile_edit(request):
    if request.method=='POST':
        form = UserProfileForm(request.POST, instance=request.user.profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/accounts/profile/')

    else:
        user=request.user
        profile=user.profile
        form=UserProfileForm(instance=profile)
    args={}
    args.update(csrf(request))
    args['form']=form
    return render_to_response('arbibets/profile_edit.html', args)

def user_profile(request):
    today=datetime.date.today()
    if request.user.profile.paid > today:
        subscription='Paid until %s ' % request.user.profile.paid
    elif request.user.profile.paid <= today:
        subscription='Currently not a paying member'
    return render(request, 'arbibets/user_profile.html',{'subscription':subscription})
    
@csrf_protect
def password_reset(request,
                   template_name='registration/password_reset_form.html',
                   email_template_name='registration/password_reset_email.html',
                   subject_template_name='registration/password_reset_subject.txt',
                   password_reset_form=PasswordResetForm,
                   token_generator=default_token_generator,
                   post_reset_redirect=None,
                   from_email=None,
                   extra_context=None,
                   html_email_template_name=None,
                   extra_email_context=None):

    if post_reset_redirect is None:
        post_reset_redirect = reverse('password_reset_done')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    if request.method == "POST":
        form = password_reset_form(request.POST)
        if form.is_valid():
            opts = {
                'use_https': request.is_secure(),
                'token_generator': token_generator,
                'from_email': from_email,
                'email_template_name': email_template_name,
                'subject_template_name': subject_template_name,
                'request': request,
                'html_email_template_name': html_email_template_name,
                'extra_email_context': extra_email_context,
            }
            form.save(**opts)
            return HttpResponseRedirect(post_reset_redirect)
    else:
        form = password_reset_form()
    context = {
        'form': form,
        'title': _('Password reset'),
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


def password_reset_done(request,
                        template_name='registration/password_reset_done.html',
                        extra_context=None):

    context = {
        'title': _('Password reset sent'),
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


# Doesn't need csrf_protect since no-one can guess the URL
@sensitive_post_parameters()
@never_cache
def password_reset_confirm(request, uidb64=None, token=None,
                           template_name='registration/password_reset_confirm.html',
                           token_generator=default_token_generator,
                           set_password_form=SetPasswordForm2,
                           post_reset_redirect=None,
                           extra_context=None):
    """
    Check the hash in a password reset link and present a form for entering a
    new password.
    """

    assert uidb64 is not None and token is not None  # checked by URLconf
    if post_reset_redirect is None:
        post_reset_redirect = reverse('password_reset_complete')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    try:
        # urlsafe_base64_decode() decodes to bytestring
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError):
        user = None

    if user is not None and token_generator.check_token(user, token):
        validlink = True
        title = _('Enter new password')
        if request.method == 'POST':
            form = set_password_form(user, request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(post_reset_redirect)
        else:
            form = set_password_form(user)
    else:
        validlink = False
        form = None
        title = _('Password reset unsuccessful')
    context = {
        'form': form,
        'title': title,
        'validlink': validlink,
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


def password_reset_complete(request,
                            template_name='registration/password_reset_complete.html',
                            extra_context=None):
    context = {
        'login_url': resolve_url(settings.LOGIN_URL),
        'title': _('Password reset complete'),
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)

@sensitive_post_parameters()
@csrf_protect
@login_required
def password_change(request,
                    template_name='registration/password_change_form.html',
                    post_change_redirect=None,
                    password_change_form=PasswordChangeForm2,
                    extra_context=None):

    if post_change_redirect is None:
        post_change_redirect = reverse('password_change_done')
    else:
        post_change_redirect = resolve_url(post_change_redirect)
    if request.method == "POST":
        form = password_change_form(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            # Updating the password logs out all other sessions for the user
            # except the current one.
            update_session_auth_hash(request, form.user)
            return HttpResponseRedirect(post_change_redirect)
    else:
        form = password_change_form(user=request.user)
    context = {
        'form': form,
        'title': _('Password change'),
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


@login_required
def password_change_done(request,
                         template_name='registration/password_change_done.html',
                         extra_context=None):
    context = {
        'title': _('Password change successful'),
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)

@login_required
def payment_successful(request):
    return render(request, 'arbibets/payment_successful.html')

@login_required
def payment_failed(request):
    return render(request,'arbibets/payment_failed.html')

def google(request):
    return render(request,'longer/google73cdd85a4713cc07.html')


@login_required
def buy_premium(request):
    user=request.user
    vrijeme=str(datetime.datetime.now())
    uid=str(user.id)
    url_template1=base64.urlsafe_b64encode((uid+','+vrijeme+','+'One month plan').encode('ascii'))
    url_template2=base64.urlsafe_b64encode((uid+','+vrijeme+','+'Three months Plan').encode('ascii'))
    url_template3=base64.urlsafe_b64encode((uid+','+vrijeme+','+'Year plan').encode('ascii'))
    # What you want the button to do.
    paypal_dict1 = {
        "business": "username@domain.com",
        "amount": "5",
        "item_name": "One month premium",
        "invoice": (url_template1),
        "notify_url": request.build_absolute_uri(reverse('paypal-ipn')),
        "return_url": request.build_absolute_uri(reverse('payment_successful')),
        "cancel_return": request.build_absolute_uri(reverse('payment_failed')),
        "custom": "One month premium",  # Custom command to correlate to some function later (optional)
    }

    paypal_dict2 = {
        "business": "username@domain.com",
        "amount": "12",
        "item_name": "Three months premium",
        "invoice": url_template2,
        "notify_url": request.build_absolute_uri(reverse('paypal-ipn')),
        "return_url": request.build_absolute_uri(reverse('payment_successful')),
        "cancel_return": request.build_absolute_uri(reverse('payment_failed')),
        "custom": "Three months premium",  # Custom command to correlate to some function later (optional)
    }
    paypal_dict3 = {
        "business": "username@domain.com",
        "amount": "40",
        "item_name": "One year premium",
        "invoice": url_template3,
        "notify_url": request.build_absolute_uri(reverse('paypal-ipn')),
        "return_url": request.build_absolute_uri(reverse('payment_successful')),
        "cancel_return": request.build_absolute_uri(reverse('payment_failed')),
        "custom": "One year premium",  # Custom command to correlate to some function later (optional)
    }
    # Create the instance.
    
    form1 = PayPalPaymentsForm(initial=paypal_dict1)
    form2 = PayPalPaymentsForm(initial=paypal_dict2)
    form3 = PayPalPaymentsForm(initial=paypal_dict3)
    context = {"form1": form1, "form2":form2, "form3": form3}
    return render(request, "arbibets/payment.html", context)

def about(request):
    return render(request,'arbibets/about.html')
    

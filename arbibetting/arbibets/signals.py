from paypal.standard.models import ST_PP_COMPLETED
from paypal.standard.ipn.signals import valid_ipn_received
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
import datetime
from django.contrib.auth.models import User
import decimal
#from arbibets.models import Kupnja
import base64
    
def show_me_the_money(sender, **kwargs):
    ipn_obj = sender
    if ipn_obj.payment_status == ST_PP_COMPLETED:
        # WARNING !
        # Check that the receiver email is the same we previously
        # set on the `business` field. (The user could tamper with
        # that fields on the payment form before it goes to PayPal)
        if ipn_obj.receiver_email != "vjenceslav88@gmail.com":
# Not a valid payment
            return
# ALSO: for the same reason, you need to check the amount
# received, `custom` etc. are all what you expect or what
# is allowed.

# Undertake some action depending upon `ipn_obj`.
        if ipn_obj.custom == "One month premium":
            price = decimal.Decimal('5.00')
            valuta='USD'
            if ipn_obj.mc_gross == price and str(ipn_obj.mc_currency) == valuta:
                #Doing this since string conversion fails in some cases
                str_invoice = str(ipn_obj.invoice)
                str_invoice = str_invoice.strip("b'")
                str_invoice = str_invoice.strip("'")
                str_invoice = bytes(str_invoice,'ascii')
                glstr=base64.urlsafe_b64decode(str_invoice)
                glstr = glstr.decode('ascii')
                uid=glstr.split(',')[0]
                vrijeme= datetime.datetime.strptime(glstr.split(',')[1], "%Y-%m-%d %H:%M:%S.%f")
                user = User.objects.get(pk=uid)
                profil=user.profile
                danas=datetime.date.today()
                if user.profile.paid < danas:
                    end_date = danas + datetime.timedelta(days=31)
                    profil.paid=end_date
                    profil.save()
                elif user.profile.paid >= danas:
                    end_date = user.profile.paid + datetime.timedelta(days=31)
                    profil.paid = end_date
                    profil.save()
                kupnja = Kupnja()
                kupnja.user = user
                kupnja.date = vrijeme
                kupnja.arbibets_invoice = str_invoice
                kupnja.paypal_invoice = str_invoice
                kupnja.length =glstr.split(',')[2]
                kupnja.save()
                
        if ipn_obj.custom == "Three months premium":
            price = decimal.Decimal('12.00')
            valuta ='USD'
            if ipn_obj.mc_gross == price and str(ipn_obj.mc_currency) == valuta:
                str_invoice = str(ipn_obj.invoice)
                str_invoice = str_invoice.strip("b'")
                str_invoice = str_invoice.strip("'")
                str_invoice = bytes(str_invoice,'ascii')
                glstr = base64.urlsafe_b64decode(str_invoice)
                glstr = glstr.decode('ascii')
                uid = glstr.split(',')[0]
                vrijeme = datetime.datetime.strptime(glstr.split(',')[1], "%Y-%m-%d %H:%M:%S.%f")
                user = User.objects.get(pk=uid)
                profil = user.profile
                danas = datetime.date.today()
                if user.profile.paid < danas:
                    end_date = danas + datetime.timedelta(days=93)
                    profil.paid = end_date
                    profil.save()
                elif user.profile.paid >= danas:
                    end_date = user.profile.paid + datetime.timedelta(days=93)
                    profil.paid = end_date
                    profil.save()
                kupnja = Kupnja()
                kupnja.user = user
                kupnja.date = vrijeme
                kupnja.arbibets_invoice = str_invoice
                kupnja.paypal_invoice = str_invoice
                kupnja.length = glstr.split(',')[2]
                kupnja.save()              
        if ipn_obj.custom == "One year premium":
            price = decimal.Decimal('40.00')
            valuta ='USD'
            if ipn_obj.mc_gross == price and str(ipn_obj.mc_currency) == valuta:
                str_invoice = str(ipn_obj.invoice)
                #Doing this since string conversion fails in some cases
                str_invoice = str_invoice.strip("b'")
                str_invoice = str_invoice.strip("'")
                str_invoice = bytes(str_invoice,'ascii')
                glstr = base64.urlsafe_b64decode(str_invoice)
                glstr = glstr.decode('ascii')
                uid = glstr.split(',')[0]
                vrijeme= datetime.datetime.strptime(glstr.split(',')[1], "%Y-%m-%d %H:%M:%S.%f")
                user = User.objects.get(pk=uid)
                profil= user.profile
                danas = datetime.date.today()
                if user.profile.paid < danas:
                    end_date = danas + datetime.timedelta(days=366)
                    profil.paid = end_date
                    profil.save()
                elif user.profile.paid >= danas:
                    end_date = user.profile.paid + datetime.timedelta(days=366)
                    profil.paid = end_date
                    profil.save()
                kupnja = Kupnja()
                kupnja.user = user
                kupnja.date = vrijeme
                kupnja.arbibets_invoice = str_invoice
                kupnja.paypal_invoice = str_invoice
                kupnja.length = glstr.split(',')[2]
                kupnja.save()   
    else:
        return
        
